@extends('backend.layout.master')
@section('content')



<section class="content">
        <div class="row">
                @if (session('success'))
                    <div class="alert alert-success">
                        <strong>{{ session('success') }}</strong>
                    </div>
                @endif
            <div class="col-md-12">
            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Thêm tin tức</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="{{ route('details.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tiêu đề tin tức</label>
                  <input type="text" class="form-control" id="" placeholder="nhập tên danh mục" name="title">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Tên không dấu</label>
                  <input type="text" class="form-control" id="" placeholder="nhập tên không dấu" name="slug">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Mô tả ngắn</label>
                  <input type="text" class="form-control" id="" placeholder="nhập tên không dấu" name="describe">
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Nội dung bài viết</label>
                    <textarea name="editor1"></textarea>
                </div>
                

                <div class="form-group">
                  <label for="exampleInputFile">Ảnh đại diện bài viết</label>
                  <input type="file" id="exampleInputFile" name="image">
                  <p class="help-block">Nhập ảnh đại diện bài viết</p>
                </div>

                <div class="checkbox">
                  <label>
                    <input type="radio" name="newhot" value="0" checked> Tin thường
                  </label>
                  <label style="margin-left: 10px;">
                    <input type="radio" name="newhot" value="1"> Tin nổi bật
                  </label>
                </div>

                <div class="form-group">
                    <label for="sel1">Thể loại</label>
                    <select class="form-control" id="cat" onchange="getChild(this)">
                    @foreach( $cat as $ca)
                        <option value="{{ $ca->cat_id }}">{{ $ca->cat_name }}</option>
                    @endforeach
                    </select>
                    
                </div>

                <div class="form-group">
                    <label for="sel1">Loại tin</label>
                    <select class="form-control" id="kind" name="kind_id">
                    @foreach( $kind as $kin)
                        <option value="{{$kin->kind_id}}">{{ $kin->kind_name }}</option>
                    @endforeach
                    </select>
                </div>


              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Lưu lại
            </form>
          </div>
            </div>
        </div>
        

</section>


@endsection

@section('script')
    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
    <script src="https://cdn.ckeditor.com/[version.number]/[distribution]/ckeditor.js"></script>
    <script>
            CKEDITOR.replace( 'editor1' );
    </script>
    <script>
            $(document).ready(function(){
                $('.alert-success').delay(4000).slideUp();
            });
    </script>

    <script>
        function getChild(opj){
          var id_cat = $(opj).val();
          $.get("/admin/kindnew/"+id_cat,function(data){
                    $("#kind").html(data);
                });
        }
    </script>
@endsection