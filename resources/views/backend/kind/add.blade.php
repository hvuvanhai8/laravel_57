@extends('backend.layout.master')
@section('content')

<section class="content">
        <div class="row">
                @if (session('success'))
                    <div class="alert alert-success">
                        <strong>{{ session('success') }}</strong>
                    </div>
                @endif
            <div class="col-md-6">
            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Thêm loại tin</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="{{ route('kind.store')}}">
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tên loại tin</label>
                  <input type="text" class="form-control" id="" placeholder="nhập tên danh mục" name="name">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Tên không dấu</label>
                  <input type="text" class="form-control" id="" placeholder="nhập tên không dấu" name="slug">
                </div>
                <div class="form-group">
                    <label>Loại danh mục</label>
                    <select multiple="" class="form-control" name="cat_id">
                    @foreach($cat as $ca)
                    <option value="{{ $ca->cat_id }}">{{ $ca->cat_name }}</option>
                    @endforeach
                    </select>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Lưu lại
            </form>
          </div>
            </div>
        </div>
</section>


@endsection

@section('script')
    <script>
            $(document).ready(function(){
                $('.alert-success').delay(4000).slideUp();
            });
    </script>
@endsection