@extends('backend.layout.master')
@section('content')

<section class="content">
        <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh sách thể loại</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th>Tên thể loại</th>
                  <th>Ngày tạo</th>
                  <th>Tên không dấu</th>
                  <th>Hành động</th>
                </tr>
                @foreach($cat as $ca)
                <tr>
                  <td>{{$ca->cat_id}}</td>
                  <td>{{$ca->cat_name}}</td>
                  <td>{{$ca->created_at}}</td>
                  <td>{{$ca->cat_slug}}</td>
                <td><button type="button" class="btn btn-block btn-success btn-sm"><a href="{{ route('category.edit', ['id' => $ca->cat_id]) }}" style="color: #fff;">Sửa</a></button>
                  <button type="button" class="btn btn-block btn-danger btn-sm"><a href="{{ route('category.destroy', ['id' => $ca->cat_id]) }}" style="color: #fff;">Xóa</a></button></td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        </div>
</section>

@endsection



@section('script')
    <script>
            $(document).ready(function(){
                $('.alert-success').delay(4000).slideUp();
            });
    </script>
@endsection