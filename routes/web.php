<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// login logout 
Route::get('/login', 'Admin\LoginController@getLogin')->name('admin.login');
Route::post('/postlogin', 'Admin\LoginController@postLogin')->name('admin.post.login');

// backend
Route::group(['prefix' => 'admin','middleware' => 'auth'], function() {

    Route::get('/dashboard', function () {
        return view('backend.index');
    });

    Auth::routes();

    //category
    Route::resource('category','Admin\CategoryController');
    Route::resource('kind','Admin\KindController');
    Route::resource('details','Admin\DetailsController');

    Route::get('/kindnew/{id}','Admin\AjaxController@getKind');
    



// logout admin
Route::get('/getlogout', 'Admin\LoginController@getLogout')->name('admin.logout');

});

// frontend
Route::get('/index', 'Frontend\PageController@getIndex')->name('home');