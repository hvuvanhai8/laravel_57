<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class OptionController extends Controller
{
    //
    public function getCategory()
    {
        $cate = Category::all();

        return view('backend.category.list', compact('cate'));
    }

    public function getAddCategory()
    {
        return view('backend.category.add');
    }

    public function postAddCategory(Request $request)
    {

        $cate = new Category;
        $cate->name = $request->name;
        $cate->highlights = $request->highlights;


        $cate->slug = $request->slug;
        $cate->save();


        return redirect()->back()->with(['success'=>' Bạn đã thêm thành công ']);
    }

    public function getEditCategory($id)
    {
        $cate = Category::findOrFail($id);
        return view('backend.category.edit', compact('cate'));
    }

    public function postEditCategory(Request $request, $id)
    {
        $cate = Category::findOrFail($id);
        $cate->name = $request->name;
        $cate->highlights = $request->highlights;
        $cate->slug = $request->slug;
        $cate->save();


        return redirect()->back()->with('success','bạn đã sửa thành công');
    }

    public function getXoaCategory($id)
    {
        $cate = Category::findOrFail($id);
        $cate->delete();

        return redirect()->back()->with('success',' Bạn đã xóa thành công ');;
    }

}
