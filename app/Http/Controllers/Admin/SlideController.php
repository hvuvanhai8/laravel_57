<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slide;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $slide = Slide::all();
        
        return view('backend.slide.list', compact('slide'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.slide.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $slide = new Slide;
        $slide->name = $request->name;

        if ($request->hasFile('image')) {

            // $url = "/uploadfile/slide";
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/slide');

            
            $image->move($destinationPath, $name);
            $slide->image = $name;
    
        }


        $slide->save();

        return redirect()->route('slide.index')->with('success','bạn đã thêm mới thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $slide = Slide::findOrFail($id);
        return view('backend.slide.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $slide = Slide::findOrFail($id);
        $slide->name = $request->name;

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploadfile/slide');

            
            $image->move($destinationPath, $name);
            $slide->image = $name;
    
        }


        $slide->save();

        return redirect()->route('slide.index')->with('success','bạn đã sửa thành công');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $slide = Slide::findOrFail($id);
        $slide->delete();

        return redirect()->back();
    }

    public function getDelSlide($id)
    {
        $slide = Slide::findOrFail($id);
        $slide->delete();

        return redirect()->back();
    }
}
