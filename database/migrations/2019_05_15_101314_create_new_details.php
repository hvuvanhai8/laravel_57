<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_details', function (Blueprint $table) {
            $table->increments('new_id');
            $table->string('new_title',255);
            $table->string('new_slug',255);
            $table->text('new_describe');
            $table->longText('new_content');
            $table->string('new_image');
            $table->integer('new_hot')->default(0);
            $table->integer('new_view')->default(0);
            $table->integer('new_kind')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_details');
    }
}
